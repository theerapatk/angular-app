export interface PokeApiPokemon {
    id: number,
    name: string,
    sprites: {
        front_default: string
    }
}